# Glassy

Glassy is an app made with MVC structure with NodeJS, AngularJS, ES6 and ExpressJS.
The main purpose of this appliaction is to get into those programming languages at class and to make a project using them.


## Getting Started

This is a simple application with some user funcionality such as Login, Register, Social Login, Listing Products and filtering them, some
interactions with the map with some shops where we catch that data from MongoDB server.


### Prerequisites

First of all you will need to execute the following commant to install what am I using in this app.

NodeJS
```
sudo apt-get install nodejs
```

NPM
```
sudo apt-get install npm
```

MongoDB
```
sudo apt-get install -y mongodb-org
```


### Installing

You will need to execute the follwing commands to run the development env once you have installed the prerequisites.

On client

```
cd frontend
npm install
```

On server

```
cd backend
npm install
```

*My app is isomorphic so the following commands will be executed on 'backend' directory:

```
gulp
```
```
npm run dev
```


## Built With

* [NodeJS](https://nodejs.org/es/) - As a backend framework
* [AngularJS](https://angularjs.org/) - As a frontend framework
* [NPM](https://www.npmjs.com/) - Dependency Management
* [Gulp](https://gulpjs.com/) - Toolkit for automating tasks
* [Browserify](http://browserify.org/) - Bundling up all dependencies
* [MongoDB](https://www.mongodb.com/es) - DB used
* [Babel](https://babeljs.io/) - JavaScript compiler


## Authors

* **Hibernon Puig** - [Puigsolerh](https://github.com/puigsolerh)


## Extra

This app is incomplete (like everyonse should be). So I could have include extra functionality such as favorite list, most rated glasses, offers, cart and so on.
