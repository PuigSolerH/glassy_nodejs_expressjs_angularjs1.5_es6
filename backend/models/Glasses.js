var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var slug = require('slug');

var GlassesSchema = new mongoose.Schema({
  slug: {type: String, lowercase: true, unique: true},
  name: String,
  desc: String,
  type: String,
  code: String,
  material: String,
  shop_by: String,
  price: String,
  img: String,
  stock: Number
}, {timestamps: true});

GlassesSchema.plugin(uniqueValidator, {message: 'is already taken'});

GlassesSchema.pre('validate', function(next){
  if(!this.slug)  {
    this.slugify();
  }

  next();
});

GlassesSchema.methods.slugify = function() {
  this.slug = slug(this.title) + '-' + (Math.random() * Math.pow(36, 6) | 0).toString(36);
};

mongoose.model('Glasses', GlassesSchema);