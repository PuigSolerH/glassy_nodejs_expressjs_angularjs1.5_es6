var mongoose = require('mongoose');

var HomeSchema = new mongoose.Schema({
  img: String,
  longitud: String,
  latitud: String,
  direc: String,
  name: String
}, {timestamps: true});


mongoose.model('Shops', HomeSchema);