var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var socialKeys = require('../credentials/credentials.json');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id)
    .then(user => {
      done(null, user);
    })
    .catch(error => {
      console.log(`Error: ${error}`);
    });
});

passport.use(new LocalStrategy({
  usernameField: 'user[email]',
  passwordField: 'user[password]'
}, function(email, password, done) {
  User.findOne({email: email}).then(function(user){
    if(!user || !user.validPassword(password)){
      return done(null, false, {errors: {'email or password': 'is invalid'}});
    }
    return done(null, user);
  }).catch(done);
}));


passport.use(new GoogleStrategy({
  clientID: socialKeys.GOOGLEPLUS_CLIENT_ID,
  clientSecret: socialKeys.GOOGLEPLUS_CLIENT_SECRET,
  callbackURL: socialKeys.GOOGLEPLUS_CALLBACK,
  passReqToCallback: true
  },
  function(request, accessToken, refreshToken, profile, done) {
    User.findOne({ 'idsocial' : profile.id }, function(err, user) {
        if (err)
          return done(err);

        // if the user is found then log them in
        if (user) {
            console.log('USER EXISTS');
            return done(null, user);
        } else {
          console.log('USUARIO NO EXISTE');
          var user = new User({
              idsocial: profile.id,
              username: profile.name.givenName,
              email: profile.emails[0].value,
              image: profile.photos[0].value,
          });
          console.log("user" + user);
          user.save(function(err) {
              if(err){
                console.log(err);
                  return done(null, user);
              }
          });
      }
    });
  }
));

//Passport strategy to connect with Facebook
passport.use(new FacebookStrategy({
  clientID: socialKeys.FACEBOOK_CLIENT_ID,
  clientSecret: socialKeys.FACEBOOK_CLIENT_SECRET,
  callbackURL: socialKeys.FACEBOOK_CALLBACK,
  profileFields: ['id', 'displayName', 'name', 'email', 'link', 'locale', 'photos'],
  passReqToCallback: true
}, function(req, accessToken, refreshToken, profile, done) {
    //Search for the user in database
    User.findOne({ 'idsocial' : profile.id }, function(err, user) {
      if (err){
        console.log('err');
        return done(err);
      }if (user) {
        return done(null, user);
      } else {
        console.log('USUARIO NO EXISTE');
          var user = new User({
              idsocial: profile.id,
              username: profile.name.givenName,
              email: profile.emails[0].value,
              image: profile.photos[0].value,
          });
          //console.log(user);
          user.save(function(err) {
            console.log(err);
              if(err){
                  console.log('USER:');
                  console.log(user);
                  return done(null, user);
              }
          });
      }
    });
  
}));//FacebookStrategy end
