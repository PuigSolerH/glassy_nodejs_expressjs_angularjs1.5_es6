var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');
var multer = require('multer');

var file = require('file-system');
var fs = require('fs');
 
file.readFile === fs.readFile // true

var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/');
  },
  filename: function (req, file, cb) {
    var datetimestamp = Date.now();
    cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
  }
});

var upload = multer({ //multer settings
  storage: storage
}).single('file');

/** API path that will upload the files */
router.post('/media/upload', auth.required, function(req, res, next) {
  upload(req,res,function(err){
    if(err){
      res.json({error_code:1,err_desc:err});
      return;
    }

    User.findById(req.payload.id).then(function(user_old){ 
      User.update({_id:req.payload.id}, { $set:{image:"uploads/"+req.file.filename}}).then(function(){
        User.findById(req.payload.id).then(function(user){
          return user.save().then(function(){
          return res.json({user: user.toAuthJSON()});
        }).catch(next);
        });
      });
      fs.unlink("public/"+user_old.image);
    });
  });
});

router.get('/user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

router.put('/user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    // only update fields that were actually passed...
    if(typeof req.body.user.username !== 'undefined'){
      user.username = req.body.user.username;
    }
    if(typeof req.body.user.email !== 'undefined'){
      user.email = req.body.user.email;
    }
    if(typeof req.body.user.password !== 'undefined'){
      user.setPassword(req.body.user.password);
    }

    return user.save().then(function(){
      return res.json({user: user.toAuthJSON()});
    });
  }).catch(next);
});

router.post('/users/login', function(req, res, next){
  
  if(!req.body.user.email){
    return res.status(422).json({errors: {email: "can't be blank"}});
  }

  if(!req.body.user.password){
    return res.status(422).json({errors: {password: "can't be blank"}});
  }

  passport.authenticate('local', {session: false}, function(err, user, info){
    if(err){ return next(err); }
    if(user){
      user.token = user.generateJWT();
      return res.json({user: user.toAuthJSON()});
    } else {
      return res.status(422).json(info);
    }
  })(req, res, next);
});

router.post('/users/register', function(req, res, next){
  var user = new User();
  user.username = req.body.user.username;
  user.email = req.body.user.email;
  user.setPassword(req.body.user.password);

  user.save().then(function(){
    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

router.post('/users/sociallogin', function(req, res, next){
  let memorystore = req.sessionStore;
  let sessions = memorystore.sessions;
  let sessionUser;
  for(var key in sessions){
    sessionUser = (JSON.parse(sessions[key]).passport.user);
  }

  User.findOne({ '_id' : sessionUser }, function(err, user) {
    if (err)
      return done(err);
    // if the user is found then log them in
    if (user) {
        user.token = user.generateJWT();
        return res.json({user: user.toAuthJSON()});// user found, return that user
      } else {
        return res.status(422).json(err);
    }
    });
});

router.get('/auth/googleplus', passport.authenticate('google', { scope: [
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.profile.emails.read'] })
);
router.get('/auth/googleplus/callback',
  passport.authenticate('google', {
   successRedirect : 'http://localhost:8081/#!/auth/sociallogin',
   failureRedirect: '/' }));

router.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email', 'public_profile']}));
router.get('/auth/facebook/callback',
      passport.authenticate('facebook',{ 
      successRedirect: 'http://localhost:8081/#!/auth/sociallogin', 
      failureRedirect: '/' }));
         
module.exports = router;
