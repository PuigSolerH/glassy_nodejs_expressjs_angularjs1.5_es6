var router = require('express').Router();
var mongoose = require('mongoose');
var Glasses = mongoose.model('Glasses');

// return a list of tags
router.get('/', function(req, res, next) {
    Glasses.find().then(function(glasses){
    return res.json({glasses: glasses});
  }).catch(next);
  //return res.json({"LLego al": "Glasses-Server"});*/
});

router.get('/:slug', function(req, res, next) {
  Glasses.findOne({ slug: req.params.slug}).then(function(glasses){
    if(!glasses){ return res.sendStatus(401); }
  return res.json({glasses: glasses});
}).catch(next);
//return res.json({"LLego al": "Glasses-Server"});*/
});

router.get('/ftype/:shop_by', function(req, res, next) {
  Glasses.find({shop_by: req.params.shop_by}).then(function(glasses){
    if(!glasses){ return res.sendStatus(401); }
    console.log("Return Glasses");
  return res.json({glasses: glasses});
}).catch(next);
//return res.json({"LLego al": "Glasses-Server"});*/
});

module.exports = router;