var router = require('express').Router(),
    swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

router.use('/', require('./users'));
router.use('/profiles', require('./profiles'));
router.use('/contact', require('./contact'));
router.use('/glasses', require('./glasses'));
router.use('/home', require('./home'));
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

router.use(function(err, req, res, next){
  if(err.name === 'ValidationError'){
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;