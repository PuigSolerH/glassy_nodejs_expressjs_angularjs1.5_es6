class SettingsCtrl {
  constructor(User, $state, Toaster) {
    'ngInject';

    this._User = User;
    this._$state = $state;

    this.formData = {
      email: User.current.email,
      bio: User.current.bio,
      image: User.current.image,
      username: User.current.username
    }

    var vm = this;

    vm.logout = User.logout.bind(User);

    vm.submitForm = function() {
        if (vm.settings_form.file.$valid && vm.file) { //check if from is valid
          vm._User.update_social(vm.file).then(
              (user) => {
                Toaster.showToaster('success','Image updated successfully ');
                this._$state.go('app.profile.main', {username:user.username});
              },
              (resp) => {
                Toaster.showToaster('error','Error status: ' + resp.status);
              },
              (evt) => {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
              }
          )
        } else {
          vm.isSubmitting = true;
          vm._User.update(this.formData).then(
            (user) => {
              this._$state.go('app.profile.main', {username:user.username})
            },
            (err) => {
              this.isSubmitting = false;
              this.errors = err.data.errors;
            }
          )
        }
      }
  }

}

export default SettingsCtrl;
