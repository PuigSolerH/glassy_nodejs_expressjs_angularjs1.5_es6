import angular from 'angular';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);


import UserService from './user.service';
servicesModule.service('User', UserService);

import JwtService from './jwt.service'
servicesModule.service('JWT', JwtService);

import ProfileService from './profile.service';
servicesModule.service('Profile', ProfileService);

import ContactService from './contact.service';
servicesModule.service('Contact', ContactService);

import GlassesService from './glasses.service';
servicesModule.service('Glasses', GlassesService);

import HomeService from './home.service';
servicesModule.service('Home', HomeService);

import ToasterService from './toaster.service';
servicesModule.service('Toaster', ToasterService);


export default servicesModule;
