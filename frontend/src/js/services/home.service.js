export default class Home {
    constructor(JWT, AppConstants, $http, $q) {
      'ngInject';
  
      this._AppConstants = AppConstants;
      this._$http = $http;
    }
  
    getAllShops() {
      return this._$http({
        url: this._AppConstants.api + '/home',
        method: 'GET',
      }).then((res) => res.data.shops);
    }
  }