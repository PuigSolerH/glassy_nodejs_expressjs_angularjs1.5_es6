export default class Glasses {
    constructor(JWT, AppConstants, $http, $q) {
      'ngInject';
  
      this._AppConstants = AppConstants;
      this._$http = $http;
    }
  
    getAll() {
      return this._$http({
        url: this._AppConstants.api + '/glasses',
        method: 'GET',
      }).then((res) => res.data.glasses);
    }
  
    gettype(type) {
        return this._$http({
          url: this._AppConstants.api + '/glasses/ftype/'+type,
          method: 'GET',
        }).then((res) => 
        // {
        //   console.log('*********************entre3******************');
        //   console.log(res.data.glasses);
        // }
        res.data.glasses
        );
    }
  
    // getGlasses(id) {
    //   return this._$http({
    //     url: this._AppConstants.api + '/glasses/'+ id,
    //     method: 'GET',
    //   }).then((res) => res.data.glasses);
    // }

        getGlasses(slug) {
      return this._$http({
        url: this._AppConstants.api + '/glasses/'+ slug,
        method: 'GET',
      }).then((res) => 
      // {
      //   console.log("glasses.service.js***********************"+JSON.stringify(res));
      // }
      res.data.glasses
      );
    }

    // getGlasses(slug) {
    //   console.log('glasses.service.js******************************'+slug);
    //   let deferred = this._$q.defer();
  
    //   if (!slug.replace(" ", "-")) {
    //     deferred.reject("Glasses slug is empty");
    //     return deferred.promise;
    //   }
  
    //   this._$http({
    //     url: this._AppConstants.api + '/glasses/'+ slug,
    //     method: 'GET'
    //   }).then(
    //     (res) =>
    //     // {
    //     //   console.log(res);
    //     // } 
    //     deferred.resolve(res.data.glasses),
    //     (err) => deferred.reject(err)
    //   );
  
    //   return deferred.promise;
    // }
  }