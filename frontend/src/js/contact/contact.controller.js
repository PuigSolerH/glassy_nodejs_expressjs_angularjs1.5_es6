class ContactCtrl {
  constructor(AppConstants, $scope, Contact, $state, Toaster) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;

    $scope.contact = {
        inputName: "",
        inputEmail: "",
        inputSubject: "",
        inputMessage: ""
    };

    $scope.SubmitContact = function () {
      var data = {
        name: $scope.vm.inputName,
        from: 'hiber98@gmail.com',
        to: $scope.vm.inputEmail,
        subject: $scope.vm.inputSubject,
        text: $scope.vm.inputMessage,
        type: 'admin'
      };
      
      Contact.sendEmail(data).then(function (response) {
        if (response) {
          Toaster.showToaster('success','Email sent correctly!');
          setTimeout(function() {
            $state.go('app.home');
          }, 2000);
        } else {
          Toaster.showToaster('error','Problem sending your email, please try again later!');
        }
      });
    };

  }
}

export default ContactCtrl;
