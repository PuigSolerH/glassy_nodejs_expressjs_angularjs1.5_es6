const AppConstants = {
  //api: 'https://conduit.productionready.io/api',
  //api: 'https://nodejs-angular1-yomogan.c9users.io:8080/api',
  //Local: http://localhost:8081
  //External: http://192.168.0.101:8081
  api: 'http://localhost:8080/api',

  jwtKey: 'jwtToken',
  appName: 'Conduit',
};

export default AppConstants;
