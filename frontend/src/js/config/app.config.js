import authInterceptor from './auth.interceptor'

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider, toastrConfig, $translateProvider, $translatePartialLoaderProvider) {
  'ngInject';

  $httpProvider.interceptors.push(authInterceptor);

  /*
    If you don't want hashbang routing, uncomment this line.
    Our tutorial will be using hashbang routing though :)
  */
  // $locationProvider.html5Mode(true);

  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'layout/app-view.html',
    resolve: {
      auth: function(User) {
        return User.verifyAuth();
      }
    }
  });

  angular.extend(toastrConfig, {
    autoDismiss: false,
    containerId: 'toast-container',
    maxOpened: 0,
    newestOnTop: true,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    preventOpenDuplicates: false,
    target: 'body'
  });

  $urlRouterProvider.otherwise('/');

  var en_translations = {
    "home" : "Home", 
  }
  
  var sp_translations = {
    "home" : "Inicio",
  }

  $translateProvider.preferredLanguage('en');

  $translateProvider.translations('en', en_translations);
  
  $translateProvider.translations('es', sp_translations);

  // $translateProvider.registerAvailableLanguageKeys(['es','en'],{
  //   'es-ES': 'es',
  //   'en-US': 'en',
  //   'en-UK': 'en'
  // });

  // $translatePartialLoaderProvider.addPart('layout');
  // $translateProvider.useLoader('$translatePartialLoader', {
  //   urlTemplate: '/src/i18n/{part}/{lang}.json'
  // });
  
  // // $translateProvider.useCookieStorage();

  // $translateProvider
  //   .determinePreferredLanguage()
  //   .fallbackLanguage('en')
  //   .useSanitizeValueStrategy('sce');
  
}

export default AppConfig;
