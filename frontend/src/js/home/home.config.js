function HomeConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.home', {
    url: '/',
    controller: 'HomeCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'home/home.html',
    title: 'Home',
    resolve: {
      shops: function(Home, $state) {
        return Home.getAllShops().then(
          (Home) => Home,
          (err) => $state.go('app.home')
        )
      },
      glasses: function(Glasses, $state) {
        return Glasses.getAll().then(
          (Glasses) => Glasses,
          (err) => $state.go('app.home')
        )
      }
    }
  });

};

export default HomeConfig;
