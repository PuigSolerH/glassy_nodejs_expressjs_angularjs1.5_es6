class HomeCtrl {
    constructor(User, shops, glasses, AppConstants, $scope, $state, NgMap) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this.filtro = '';
    this._$state = $state;
    this.lat = 0;
    this.lng = 0;
    this.showMap = true;
    this.shops = shops;
    this.Glasses = glasses;
    this.Home = [];
    var vm = this;

    NgMap.getMap().then(function(map) {
      vm.map = map;
    });

    // Set current list to either feed or all, depending on auth status.
    this.listConfig = {
      type: User.current ? 'feed' : 'all'
    };

    $scope.getMarkers = function() {
      vm.positions = [];
      var shop = vm.shops;
      if(shop.length > 0){
        var centerLat = 0;
        var centerLong = 0;
        for (var i = 0; i < shop.length; i++) {
          var latitude = shop[i]['latitud'];
          var longitude = shop[i]['longitud'];
          centerLat += parseFloat(latitude);
          centerLong += parseFloat(longitude);

          var id = shop[i]['_id'];
          var name = shop[i]['name'];
          var direc = shop[i]['direc'];
          var imagen = shop[i]['img'];
          var pos = {
            id: id,
            name: name,
            direc: direc,
            imagen: imagen,
            pos:
              [parseFloat(latitude),
              parseFloat(longitude)]
            }
          vm.positions.push(pos);
        }
        centerLat = parseFloat(centerLat) / shop.length;
        centerLong = parseFloat(centerLong) / shop.length;
        vm.lat  = centerLat;
        vm.lng = centerLong;
      }
    }

    $scope.ShowHideMap = function() {
      let btnShowMap = document.getElementById('btnShowMap');
      if(vm.showMap == true){
        vm.showMap = false;
      }else if (vm.showMap == false) {
        vm.showMap = true;
      }
    }

    $scope.showDetailsOnMap = function(e, Home) {
      vm.Home = Home;
      vm.map.showInfoWindow('myInfoWindow', Home.id); 
    };

    $scope.fillTextbox = function(string){
      vm.filtro = string;
    }

  }
}
export default HomeCtrl;