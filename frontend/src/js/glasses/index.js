import angular from 'angular';

// Create the module where our functionality can attach to
let glassesModule = angular.module('app.glasses', []);

// Include our UI-Router config settings
import GlassesConfig from './glasses.config';
glassesModule.config(GlassesConfig);


// Controllers
import GlassesCtrl from './glasses.controller';
glassesModule.controller('GlassesCtrl', GlassesCtrl);

import DetailGlassesCtrl from './glassesdetails.controller';
glassesModule.controller('DetailGlassesCtrl', DetailGlassesCtrl);

export default glassesModule;
