function GlassesConfig($stateProvider) {
  'ngInject';

  $stateProvider

  .state('app.glasses', {
    url: '/glasses/ftype/:filter',
    controller: 'GlassesCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'glasses/glasses.html',
    title: 'Glasses',
    resolve: {
      glasses : function(Glasses, $state){
        return Glasses.getAll().then(
          (Glasses) => Glasses,
          (err) => $state.go('app.home')
        )
      }
    }
  })

  $stateProvider
  .state('app.detailglasses', {
    url: '/glasses/:slug',
    controller: 'DetailGlassesCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'glasses/glassesdetails.html',
    title: 'Detail Glasses',
    resolve: {
      glasses_details: function(Glasses, $state, $stateParams) {
        return Glasses.getGlasses($stateParams.slug).then(
          (Glasses) => Glasses,
          (err) => $state.go('app.home')
        )
      }
    }
  })

};

export default GlassesConfig;
