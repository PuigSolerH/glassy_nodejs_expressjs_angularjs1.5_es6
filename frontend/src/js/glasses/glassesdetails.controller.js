class DetailGlassesCtrl {
    constructor(glasses_details, AppConstants, $scope) {
      'ngInject';
  
      this.appName = AppConstants.appName;
      this._$scope = $scope;
      this.glasses_details = glasses_details;
  
    }
  }
  
  export default DetailGlassesCtrl;