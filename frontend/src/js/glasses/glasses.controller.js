class GlassesCtrl {
    constructor(glasses,AppConstants, $scope, $stateParams, $filter) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this.currentPage = 1;
    this.itemsPerPage = 3;
    this.maxSize = 5; //Number of pager buttons to show
    this.filter = $stateParams.filter;
    this.Glasses = [];

    if (this.filter == null) {
      this.varInfo = glasses; 
      this.varTot = glasses;
      this.varPager = glasses;
    } else {
      this.varInfo = $filter('filter')(glasses,{shop_by:this.filter});
      this.varTot = $filter('filter')(glasses,{shop_by:this.filter});
      this.varPager = $filter('filter')(glasses,{shop_by:this.filter});
    }

    var vm = this;

    this.pageChanged = function() {
      var startPos = (vm.currentPage - 1) * vm.itemsPerPage;
      vm.varInfo = vm.varTot.slice(startPos, startPos + vm.itemsPerPage);
    };

     this.changed = function(){
      vm.varPager = $filter('filter')(vm.varTot,{name:this.search.name});
    }

    this.reset = function(){
      vm.filter = null;
      vm.varInfo = glasses; 
      vm.varTot = glasses;
      vm.varPager = glasses;
    }

  }
}

export default GlassesCtrl;