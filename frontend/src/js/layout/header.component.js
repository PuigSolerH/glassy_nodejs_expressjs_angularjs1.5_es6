class AppHeaderCtrl {
  constructor(AppConstants, User, $scope , $translate, $translatePartialLoader) {
    'ngInject';

    this.appName = AppConstants.appName;
    this.currentUser = User.current;
    // $translatePartialLoader.addPart('layout');
    // $translate.refresh();
    

    $scope.$watch('User.current', (newUser) => {
      this.currentUser = newUser;
    })
    
    this.logout = User.logout.bind(User);

    this.changeLanguage = function(key) {
      console.log(key);
      $translate.use(key);
    }

  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  templateUrl: 'layout/header.html'
};

export default AppHeader;
